# Configuration Scripts in Bash

OCI enables operators to automate the process of building infrastructure- and application services. Instead of building hardware prior to deploying software, operators iterate deployment options and optimize the entire stack before provisioning hardware. Configuration files represent the programs that launch the respective systems on demand. The installation of software is merged with the deployment of the operating system and combined with the hardware provisioning process. Modular scripts allow operators to customize deployment templates without rewriting the entire code. This introduction shows how to create configuration files, without repeating the content of shell scripting tutorials like the [Linux Foundation Training](https://training.linuxfoundation.org/training/introduction-to-linux/), [Ryan's Tutorial](https://ryanstutorials.net/bash-scripting-tutorial/), or [Oracle's University Course for DBA](https://www.oracle.com/technical-resources/articles/linux/saternos-scripting.html). 

## Syntax

Bash scripts use syntax like programming languages, but with a command set that reflects the capabilities of the operating system. Every command contains a manual that can is accessed using either the **man** or **help** argument. 

Commands can be executed in isolation (like "ls"), can be extended using arguments ("ls -la") and include a file system target ("ls -la /home/user"). Multiple commands can be combined using a pipe "|" or and "&&". Code can be shortened using special characters ("ls -la ~") or system variables ("$HOME"). 

```
command [ARGUMENT]... [FILE]...
```
Writing scripts, we can combine commands with conditionals like 'if', 'else' and 'break'; use controls like 'if..then..else'; and define 'for' or 'while' loops. We ensure modularity in bash, employing [functions](https://ryanstutorials.net/bash-scripting-tutorial/bash-functions.php). The syntax is very similar to the orginal bash code. 

```
function () {
  command [ARGUMENT]... [FILE]...
}
```

These modules are stored in libraries. We seperate helper- and admin functions, from install- and remote functions. This allows to split the work of writing and maintaining functions between engineers with different responsibility. Most of the ongoing development efforts focus on writing install functions, therefor all install functions a aggregate in one library.

## The WebShell Script

The [webshell_ol77.sh](artifacts/bash/webshell_ol77.sh) script that we used to build the local server is based on [Oracle Linux 7.7](https://blogs.oracle.com/linux/oracle-linux-iso-images-download-options) using the [bourne-again shell (bash)](https://www.tldp.org/LDP/abs/html/index.html). Like the OCI [cloudshell](https://docs.cloud.oracle.com/en-us/iaas/Content/API/Concepts/cloudshellintro.htm), the webshell exposes a SSH terminal via HTTPS. But it automates the installation of individual deployment tools.

* We update the operating system, install required packages and set basic environment variables,
* secure access by creating a new admin, and disable ssh access for the default user,
* create home directory for the new admin user, set permissions and SSH policies in SELinux, 
* download software that is not covered by Oracle's EPEL license,
* install the Infracoder toolset, the OCI command line interface and the cockpit repository and
* replicate the InfraCoder git repository.

### File Structure

An install script is outlined in three major sections. In the first section we define  modules with admin and install functions, the second section uses the main() function to compose a service instance and the third section contains the trigger to execute the installation procedure. 

```
#!/bin/bash

<service header>
<settings and variables>

# --- sources ---
source libadmin
source libinstall
source libcontrol

# --- modules ---
main () {
  function_1
  function_2
  ...
}

# --- execution ---

init
main

exit 0
```

The core script contains just two commands "init" and "main". The init command refers to a script that updates the operating system and the main() function that appoints the modules to be used assembling the the service.

### Header Information

A common header describes the service and the deployment context. The information is stored as *comments*. In bash, the '#' sign marks a line that will not be executed. We use colon as delimiter header informations can be extracted programatically.

```
## Service: <name>
## Operator: <organization>
## Location: <URL>
## Image: Oracle Linux 7.7
## Version: <0.0> 
## Tags: [...]
```

### Settings and Variables

In the settings section defines a couple of variables. Using a NAME with the equals sign defines a varaiable in bash (*VARIABLE=input*). We use CAPITAL letters for variable names, to distinguish variables from functions. Variables cache input and sepearate fixed from dynamic code. It can capture any sort of input, like numbers, strings arrays or a path. Using 'export' in front of a variable declares an **environmental variable** that is inherited across scripts. 'declare' creates an array that contains multiple values. The '-a' argument refers to an 'indexed array'. We also use variables for the credentials, however store USERNAME and PASSWORD in an external file, .credentials. With 'source' we merge the code that resides in the seperate .credentials file with the configuration file.

```
# --- settings and variables ---
export LANGUAGE=$(echo $LANG)
export LC_ALL=$(echo $LANG)

readonly SCRIPTNAME=$(basename $0)
readonly YUM_OPTS="-d1 -y"

declare -a PORTS
PORTS+=(22 80)

CREDS=${HOME}/.credentials
```

### Root Confirmation

We start with a check, whether a script is executed as 'super user' and exit if not. In Linux sudo'ers receive the user ID '0'. Using conditionals enables engineers to depend the dynamic execution of tasks on inputs like a variable or an output from a command. With a so called [internal variable](https://www.tldp.org/LDP/abs/html/internalvariables.html) 'EUID' we test the condition, wehther the effective ID is different from 0, and if the statement '[$EUID -ne 0]' returns 'true', we exit the script without further execution. The echo command is used to inform the command line user, why the script will not be executed.

```
## --- check root ---
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi
```

### Sources

Sources merge external code into the script. We store functions in libraries and use the source command to reference the code in configuration files. Infracoder libraries are reusable code that is maitained accross project. This leads to consistency in projects and improved quality. helper() and admin() are stored in libadmin.sh, install() in libinstall.sh and remote() in liboci.sh. Admin functions represent a consistent framework that can be used for multiple services, install functions proven procedures to add software. Remote functions is collection of preconfiguerd oci commands to ease the use of requests against the cloud controller.

## Admin Functions

Admin functions provide a common framework for installation procedures that can be used accross configuration files. These functions work accross versions and accross different distributions that derive from the same operating system kernel. Functions, that where written for Oracle Linux, will work on Red Hat, CentOS and Fedora. Independent from any application software, admin functions address the basic requirements for a hosted service. When creating a new service, admin functions can be reused. We employ the following admin functions:

* admin_user() to create a new admin user
* revoke_access() to revoke access for default user
* enable_communication() to adjust the local firewall settings

The first function creates an individual admin user. After grepping the user provided name we employ a similar condition than before to ensure that the new admin name is unique. We use a [Command Line Argument](http://linuxcommand.org/lc3_wss0120.php) '$?' to retrieve the input and checking whether a UID equals '0' before executing the *useradd* command. After creating the password with "perl -e 'print crypt($ARGV[0], "password")' $PASSWORD" we add the user to the [sudoer group](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux_OpenStack_Platform/2/html/Getting_Started_Guide/ch02s03.html). We end the function with adding a key for SSH access. The procedures distinguishes between a local and an OCI deployment.

```
admin_user (){
    egrep "^$USERNAME" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo "$USERNAME exists!"
		exit 1
	else
	  SECRET=$(perl -e 'print crypt($ARGV[0], "password")' $PASSWORD)
	  useradd -m -p $SECRET $USERNAME
	  [ $? -eq 0 ] && write_log "${USERNAME} has been added to system as new admin user!" || write_log "Failed to add a user!"
	fi
    
  usermod -aG wheel $USERNAME
  mkdir /home/$USERNAME/.ssh && sudo chown $USERNAME:$USERNAME /home/$USERNAME/.ssh/
    
  SSHKEY=/home/opc/.ssh/authorized_keys
  
  if [ -f "$SSHKEY" ]; then
    cp /home/opc/.ssh/authorized_keys /home/$USERNAME/.ssh/authorized_keys && sudo chown $USERNAME:$USERNAME /home/$USERNAME/.ssh/authorized_keys
  else 
    touch /home/$USERNAME/.ssh/authorized_keys && sudo chown $USERNAME:$USERNAME /home/$USERNAME/.ssh/authorized_keys
    cat ~/.ssh/id_rsa.pub | cat >> .ssh/authorized_keys
    write_log  "SSH Key copied"'
  fi

  chmod 700 /home/$USERNAME/.ssh/ && sudo chmod 600 /home/$USERNAME/.ssh/authorized_keys
  echo "AllowUsers $USERNAME" >> /etc/ssh/sshd_config
  restorecon -R -v /home/$USERNAME/.ssh
}
```

With the admin user we also generate a default **SSH key pair** for server deployments. The [step-by-step guide](https://docs.cloud.oracle.com/iaas/Content/GSG/Tasks/creatingkeys.htm?Highlight=ssh%20keys) on how to create and activate an ssh key. And we revoke the SSH access for the default user. This is a small security measure that avoids the problem of  user names being retireved from public manuals.

```
revoke_access () {
    if id -u "training" >/dev/null 2>&1; then
        sudo userdel -Z -f -r training
        clear
        echo "installation completed, please logout, open the browser and login with the new user"
    else
        clear
        echo "installation completed, please logout, open the browser and login with the new user"
    fi
}
```

Enabling communication open the communication ports for the installed applications on the instance level. It is like a second guess to security groups and lists. While the cloud controller maintains security policies for a group of machines, this measure addresses individual machines. We rely on the default firewall daemon for this configuration. It is a perfect example for employing **Loops**. Instead of creating five tasks for opening five ports on the firewall, we create a single task and reduce the PORTS array to it's unique values before repeating the task with with the ports that need to be open.

```
enable_communication () {
    OPEN_PORTS=($(echo "${PORTS[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
    for PORT in "${OPEN_PORTS[@]}"; do
        firewall-cmd --zone=public --add-port=$PORT/tcp --permanent;
    done
    firewall-cmd --reload
    write_log "Ports ${OPEN_PORTS[*]} opened"
}
```
Configuration scripts are stored and executed from the home directory of the admin user. Using the ".sh" extension executing with **./command** prevents any conflicts with operating system commands. When creating [golden images](https://opensource.com/article/19/7/what-golden-image) we write scripts that need to be avaialble system wide. These scripts are stored at "/usr/local/bin". We have to consider the naming to avoid conflicts with operating system commands. The "which <command>" helps to investigate whether a script name has already been used and "type <command>" provides information how the existing command is exposed in the system.

### Helper Functions

After that we extend the default command set with customized functionality. Helper functions are script specific and can be invoked regardless of the base operating system. An example is the write_log function that we use to catpture the status of the installation process in  a file. 

```
echo_log() {
    echo "--- ${SCRIPTNAME}: $@";
    write_log "--- ${SCRIPTNAME}: $@";
}

write_log () {
    echo ["${USER}"] [`date`] - ${*} | sudo tee -a /var/log/infracoder.log
}
```

The **init() function** is a specific helper function. It prepares the operating system to inherit service functionality. The init function is operating system and version specific. For Oracle Linux 7.7, we rely on "yum" as package manager to clear the cash, update installed packages and add packages that are required for the install modules. Running an update before installing any software corrects bugs and eliminates known vulnaribilities. For 'install' and 'update' we use the '-y' argument to surpress any user confirmation before excuting the command.

```
init (){
    yum clean all
    yum makecache fast
    yum -y update
    yum -y install jq wget unzip policycoreutils-python oci-utils nmap python3
    write_log "All packages have been updated, addtionanl packages installed"
}
```

Note: 'yum -y update' only corrects installed packages, `yum -y upgrade` is another option to update a system, but this option also deletes obsolete packages. From an automation perspective, this can be dangerous, as deleting packages through the package manager can impact the functionality of applications that were installed manually. This means from time to time engineers need to rebuild their base images to get rid of obsolet packages.

## Install Functions

Install functions contain the installation procedures for software packages. In modern linux distributions the line between operating system and application code is blurring. The desirre is, to use the package manager for installation procedures whereever possible. Which means we use the 'yum' comand not just for operating system packages but also for adding applications to the system. The package manager automatically resolve dependencies during the installation and allows to automate updates automatically - a prerequisiste to work with immutable server. Using a commercial distribution and sourcing packages from the image provider ensures that the service is covered with a third-level support agreement. Here is an example for the cockpit application, which is available in the Oracle linux EPEL repository. 

```
install_cockpit () {
    PORTS+=(443)
    yum -y install cockpit
    sed -i 's;'9090';'443';' /usr/lib/systemd/system/cockpit.socket
    semanage port -m -t websm_port_t -p tcp 443
    sudo systemctl enable --now cockpit.socket
    write_log "Cockpit installed and running on Port 443"
}
```

The install function starts with adding the communication port to the PORTS list, the array that we use to adjust the firewall. Than we install the application and use **Streaming EDitor (sed)** to change listening port to the desired HTTPS port, which enforces transport layer security between the browser and the server. sed parses and transforms text with a simple, compact programming language to search, find, replace, insert and/or delete text snippets. In this example sed substitutes the number '443' with '9090' in the 'cockpit.socket' file. We finish the function with a log statement.

In shell scripting, customizing configuration files is a typical task and Linux provides a couple of **text processing tools** like:

* [sed](https://www.grymoire.com/Unix/Sed.html) to modify text snippets
* [tr](https://www.howtoforge.com/linux-tr-command/) to work on characters, e.g. changing lower- to upper case letters
* [awk](https://www.grymoire.com/Unix/Awk.html) to change records with fields identified by lines

Not all software we require is avaialble through repositories. Another way to obtain application code in form of binaries is wget. The tool is included in most linux distributions, it supports downloads via HTTP, HTTPS, and FTP. After the download, archives are decompressed and stored in the execution directory. Terraform is an example for an application that is retrieved as binary.

```
install_terraform() {
    TF_RELEASE="0.12.20"
    cd /tmp
    wget https://releases.hashicorp.com/terraform/$TF_RELEASE/terraform_${TF_RELEASE}_linux_amd64.zip
    unzip -o ./terraform_${TF_RELEASE}_linux_amd64.zip -d /usr/local/bin/
    write_log "Terraform Version ${TF_RELEASE} installed"
}
```
Adding the '-o' argument to the unzip command enforces and override, should the target directory already contain a different version of terraform.

The last example is the installation of the oci_cli. It is a Python program and the developer, Oracle, maintains an own installation procedure. Instead of rebuilding the script we download the predefined procedure and execute the script locally.

```
install_cli () {
    cd /home/$USERNAME
    wget https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh 
    chmod +x install.sh
    chown $USERNAME:$USERNAME /home/$USERNAME/install.sh 
    sudo -H -u $USERNAME bash -c './install.sh --accept-all-defaults'
    write_log "OCI CLI installed"
}
```

### Main Function

The 'main' function executes the script. It is used to select the modules that will be installed the modules with the webshell. We always start with creating a new admin account, install the software and finish with enbaling the communinaction and cleaning the system.

```
main () {
    admin_user
    install_cockpit
    install_git
    install_packer
    install_terraform
    install_cli
    enable_communication
    revoke_access
}
```

Like every script conifguration files finish with an exit statement. Usually the exit number provides information back to an admin user, e.g. it can refer to errors that occured during execution. Server configurations run unattended, therefore we close the script with 'exit 0'.

### Testing bash scripts

In order check whether the execution of a particular script we can start is with "bash -x <command>". The -x option starts the shell in tracing mode and shows all the details of how a script is processed.  

[<<](setup.md) | [+](setup.md) | [>>](cli.md)