data "oci_identity_availability_domain" "ad" {
  compartment_id = var.tenancy_ocid
  ad_number      = 1
}

data "oci_functions_applications" "test_applications" {
  #Required
  compartment_id = var.compartment_ocid

  #Optional
  display_name = "example-application"
  id           = oci_functions_application.test_application.id
  state        = var.application_state
}


data "oci_functions_functions" "test_functions" {
  #Required
  application_id = oci_functions_application.test_application.id

  #Optional
  display_name = "example-function"
  id           = oci_functions_function.test_function.id
  state        = "AVAILABLE"
}

data "oci_apigateway_gateways" "test_gateways" {
  #Required
  compartment_id = var.compartment_ocid

  #Optional
  display_name = var.gateway_display_name
  state        = var.gateway_state
}

data "oci_apigateway_deployments" "test_deployments" {
  #Required
  compartment_id = var.compartment_ocid

  #Optional
  display_name = var.deployment_display_name
  gateway_id   = oci_apigateway_gateway.test_gateway.id
  state        = var.deployment_state
}