variable "compartment_ocid" {
  description = "The compartment identifier for the subnet"
  type        = string
}

variable "vcn_id" {
  description = "The VCN for the subnet"
  type        = string
}

variable "sl_display_name" {
  description = "Display name for the security list"
  type        = string
}

variable "egress_destination" {
  description = "Destination CIDR for the egress rule set"
  type        = string
}

variable "egress_protocol" {
  description = "Protocols for the egress rule set"
  type        = string
}

variable "ingress_stateless" {
  description = "Stateless rules true or false"
  type        = string
}

variable "ingress_rules" {
  description = "Security rule set for ingress traffic"
  type        = list
}