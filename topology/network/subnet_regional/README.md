# Regional Subnet

```
module "regional_subnets" {
  source               = "git::https://gitlab.com/tboettjer/infracoder.git//topology/network/subnet_regional/"
  cidr_block           = "10.0.0.0/24"
  compartment_ocid     = var.compartment_ocid
  vcn_id               = oci_core_vcn.main_vcn.id
  subnet_display_name  = "regional_subnet"
  subnet_dns_label     = "subnet"
}
```