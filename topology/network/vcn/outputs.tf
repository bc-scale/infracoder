output "vcn_id" {
  description = "Oracle Cloud Identifier for the VCN"
  value       = oci_core_vcn.main_vcn.id
}

output "internet_gateway_id" {
  description = "Oracle Cloud Identifier for the internet gateway. "
  value       = oci_core_internet_gateway.main_ig.id
}

output "default_route_table_id" {
  description = "Oracle Cloud Identifier for the default route table. "
  value       = oci_core_vcn.main_vcn.default_route_table_id
}

output "default_security_list_id" {
  description = "Oracle Cloud Identifier for the default security list. "
  value       = oci_core_vcn.main_vcn.default_security_list_id
}

output "default_dhcp_options_id" {
  description = "Oracle Cloud Identifier for the default DHCP options. "
  value       = oci_core_vcn.main_vcn.default_dhcp_options_id
}

output "main_security_list_id" {
  description = "Oracle Cloud Identifier for the main security list. "
  value       = module.main_security_list.list_id
}

output "main_subnet_id" {
  description = "Oracle Cloud Identifier for the main security list. "
  value       = module.regional_subnets.subnet_id
}

