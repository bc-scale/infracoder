variable "tenancy_ocid" {
#  default = var.tenancy_ocid
}
variable "compartment_ocid" {
#  default = var.compartmend_ocid
}
variable "ssh_public_key" {}
variable "instance_shape" {}
variable "image_ocid" {}
variable "availability_domain" {}
variable "network_id" {}
variable "nodePool_Subnet_1" {}
variable "nodePool_Subnet_2" {}
variable "nodePool_Subnet_3" {}

variable "cluster_name" {
  default = "OKE-cluster"
}
variable "cluster_kubernetes_version" {
  # default = "v1.15.7"
  # older version for svcatalog (possible workaround)
  default = "v1.14.8"
}
variable "cluster_options_add_ons_is_kubernetes_dashboard_enabled" {
  default = "true"
}
variable "cluster_options_add_ons_is_tiller_enabled" {
  default = "false"
}
#variable "pods_cidr" {
#
#}
#variable "services_cidr" {
#
#}

variable "node_pool_name" {
  default = "OKE-nodepool"
}
variable "node_pool_node_shape" {
  default = "VM.Standard2.1"
}
# OCID for OL 7.7 in FRA
variable "node_image_id" {
  default = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaa4cmgko5la45jui5cuju7byv6dgnfnjbxhwqxaei3q4zjwlliptuq"
}
## Insert your own public SSH key for the worker nodes here
variable "node_pool_ssh_public_key" {
  default = ""
}
