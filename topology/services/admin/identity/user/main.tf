// Copyright (c) 2017, 2019, Oracle and/or its affiliates. All rights reserved.

######################################################################################
# Creating an user, adding an api key, defining auth tokens and customer secret keys #
######################################################################################

# user
resource "oci_identity_user" "default-user" {
  name           = "User_1"
  description    = "Default user created by terraform"
  compartment_id = "var.tenancy_ocid"
}

data "oci_identity_users" "default-user" {
  compartment_id = "oci_identity_user.default-user.compartment_id"

  filter {
    name   = "name"
    values = ["User_1"]
  }
}

# password
resource "oci_identity_ui_password" "default-pwd" {
  user_id = "oci_identity_user.default-user.id"
}

# api key
resource "oci_identity_api_key" "api-key1" {
  user_id = "oci_identity_user.default-user.id"

  key_value = <<EOF
...
EOF
}

# AuthToken
resource "oci_identity_auth_token" "auth-token1" {
  user_id     = "oci_identity_user.default-user.id"
  description = "Default user auth token created by terraform"
}

resource "oci_identity_customer_secret_key" "customer-secret-key1" {
  user_id      = "oci_identity_user.default-user.id"
  display_name = "WebApp-example-customer-secret-key"
}

data "oci_identity_customer_secret_keys" "customer-secret-keys1" {
  user_id = "oci_identity_customer_secret_key.customer-secret-key1.user_id"
}