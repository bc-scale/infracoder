# Autonomous Database (ADB)

## --- Autonomuous Database ---
module "adb" {
  source            = "git::https://gitlab.com/tboettjer/infracoder.git//topology/data/adb/"
  compartment_ocid  = var.compartment_ocid
  type              = "DW"                    # OLTP or DW
  db_name           = "servicedb"
  display_name      = "service_db"
}