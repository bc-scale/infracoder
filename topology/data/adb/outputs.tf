## --- modules outputs ---
output "password" {
  value = random_string.admin_password.result
}

output "service_console_url" {
  value = oci_database_autonomous_database.adb.service_console_url
}