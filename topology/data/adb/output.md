## --- autonomuous database ---
output "password" {
  value = module.adb.password
}

output "service_console_url" {
  value = module.adb.service_console_url
}
