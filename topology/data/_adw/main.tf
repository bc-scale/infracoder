resource "random_string" "autonomous_data_warehouse_admin_password" {
  length      = 16
  min_numeric = 1
  min_lower   = 1
  min_upper   = 1
  min_special = 1
}

resource "oci_database_autonomous_database" "autonomous_data_warehouse" {
  #Required
  admin_password           = random_string.autonomous_data_warehouse_admin_password.result
  compartment_id           = var.compartment_ocid
  cpu_core_count           = "1"
  data_storage_size_in_tbs = "1"
  db_name                  = "adbdw1"

  #Optional
  db_workload             = var.autonomous_data_warehouse_db_workload
  display_name            = "example_autonomous_data_warehouse"
  freeform_tags           = var.autonomous_database_freeform_tags
  is_auto_scaling_enabled = "false"
  license_model           = var.autonomous_database_license_model
}

# resource "random_string" "autonomous_data_warehouse_wallet_password" {
#   length  = 16
#   special = true
# }

# resource "local_file" "autonomous_data_warehouse_wallet_file" {
#   content_base64 = data.oci_database_autonomous_database_wallet.autonomous_data_warehouse_wallet.content
#   filename       = path.module/autonomous_data_warehouse_wallet.zip
# }


