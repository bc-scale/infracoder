# Infrastructure as Code on Oracle Cloud Infrastructure

## Introduction

[Oracle Cloud Infrastructure (OCI)](https://www.oracle.com/cloud/) is a second generation infrastructure as a service (IaaS) offering that combines the convenience of a cloud with the ability to control own resources. Dedicated infrastructure, like compute, storage and network empower logical resources, like hypervisors, container or network functions and higher level services like autonomous databases or software appliances. More than 20 data center regions enable operators to deploy applications close to a user and address regional privacy regulations.

[<img src="docs/ociregions.png" width="500">](https://www.oracle.com/cloud/architecture-and-regions.html)

In OCI virtual cloud networks (VCN) represent isolated Layer 3 networks that allow operators to build private clouds on shared infrastructure. Compartments are administration domains that provide a choice between subscribing to a software service, outsourcing application management or mixing both in a managed service. Combining compartments and VCN introduces a very granular approach towards multi-tenancy and prevents complex management procedures. Automation scripts encompass entire solution stacks from the application to the infrastructure layer combining enterprise service hosts, with master-slave deployments and cloud-native container in a service mesh.

## Agility in Operations
Infrastructure coding is an effective way to manage service provider resources with machine readable files. Server configurations are captured in procedural scripts, transformed into artifacts and merged into solution topologies. The process allows operators to become more agile and address the requirements of modern SCRUM oriented software development process as much as the constraints of the more traditional waterfall procedures that are still required to manage most enterprise services. We combine [Command Line Interface (CLI)](https://docs.cloud.oracle.com/iaas/Content/API/Concepts/cliconcepts.htm) scripts for remote configuration with state-aware deployment plans and utilize modules to foster reusasibility and quality improvements for a continuous deployment process that spans regions, data centers and host types.

[<img src="docs/process.png"  width="500">](http://bit.ly/2pJJn2F)

With this repository we aim to provide a starting point for operations engineers introducing infrastructure as code to extend on-prem infrastructure with ad-hoc resources, build virtual data center or expose business critical data securely in the digital ecosystem. We focus on providing guidance on writing scripts for deployment automation, service modelling and resources configuration. A more detailed introduction for OCI resources and services can be found in a [seperate guide](http://bit.ly/2rujPXT).

1.  [Getting Started](docs/setup.md)
2.  [Configuration Scripts](docs/bash.md)
3.  [Remote Management](docs/cli.md)
4.  [Resource Discovery](docs/terraform.md)
5.  [Code Sharing](docs/git.md)
6.  [Custom Artifacts](docs/packer.md)
7.  [Deployment Plans](docs/modules.md)

Splitting the codebase in configuration-, artifact- and topology-definitions enables continuous deployments with a separation of concerns between application and infrastructure management. Configuration files are developed in close collaboration with application managers, artifact and topology templates enable infrastructure operators to maintain a standardized set of monitoring and management tools.

[<img src="docs/curriculum.png" width="500">](http://bit.ly/2pJJn2F)

The default choice for configuration scripts is [bash](https://learning.oreilly.com/videos/linux-shell-scripting/9781789800906) and Python. [Packer](https://www.packer.io/) assembles images for cross-region deployments and [Terraform](https://www.terraform.io/) helps to write deployment plans. In bash, the [OCI utilities](https://docs.cloud.oracle.com/iaas/Content/Compute/References/ociutilities.htm) and streaming editors like *SED* or *jq* enhancing the standard command set. The Python SDK is used to customize provider and thirdparty services through advanced scripting and API management. Packer and Terraform share the same configuration language, HCL. An OCI [Service Provider](https://github.com/terraform-providers/terraform-provider-oci) translates HCL into state-aware plans that configure, add, change or delete resources and services based on the delta between existing and desired resources. In case of a topology change, Terraform determines deviations from the current state and executes incremental plans.

[<<](docs/git.md) | [+](docs/setup.md) | [>>](docs/setup.md)
