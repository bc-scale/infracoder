#!/bin/bash

## Service: Update Infracoder
## Operator: Oracle
## Location: <URL>
## Software: https://gitlab.com/tboettjer
## Image: Oracle Linux 7.7
## Version: <0.2> 
## Tags: [...]

INFRA_SETUP=/usr/local/bin/setup
INFRA_INFRACODER=/usr/local/bin/infracoder

if [ -f "$INFRA_SETUP" ]; then
    mv $INFRA_SETUP $INFRA_INFRACODER
fi

if [ -f "$INFRA_INFRACODER" ]; then
    sed -i 's/EMAIL=$1/EMAIL=$2/' $INFRA_INFRACODER
    sed -i 's/CONFIGURATION=$2/CONFIGURATION=$3/' $INFRA_INFRACODER
fi

# enabling the infracoder command for sudoers
sed -i 's;secure_path = /sbin:/bin:/usr/sbin:/usr/bin;secure_path = /sbin:/bin:/usr/sbin:/usr/local/bin:/usr/bin;' /etc/sudoers

exit 1