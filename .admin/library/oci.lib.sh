#!/bin/bash

## Service: Remote Library
## Operator: Oracle
## Location: <URL>
## Software: https://gitlab.com/tboettjer
## Image: Oracle Linux 7.7
## Version: <0.2> 
## Tags: [MGT]

# --- create functions ---
configure_cli() {
    echo "Configuring the CLI"
    oci iam compartment list --output table --query 'data[0].{Tenancy: "compartment-id"}';
    oci iam user list --all --output table --query 'data[].{Name: "name", OCID: "id"}';
    oci setup config;
}

select_dns_zone() {
    while true; do
        read -p "Do you want to create a new zone file (y/n)? " yn
        case $yn in
            [Yy]* ) create_dns_zone; break;;
            [Nn]* ) configure_dns_zone; return 1;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

create_dns_zone() {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    COMPARTMENT=$(jq -r '.client.configuration[] | .compartment' ${OCLOUD_CONFIG});
    read -e -p "Please a zone name (your_name.eu.org): " FQDN;

    oci dns zone create  \
    --compartment-id $COMPARTMENT \
    --name $FQDN \
    --zone-type PRIMARY \
    --wait-for-state ACTIVE;

    configure_dns_zone;
}

configure_dns_zone() {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    COMPARTMENT=$(jq -r '.client.configuration[] | .compartment' ${OCLOUD_CONFIG});
    PROJECT=$(jq -r '.client.configuration[] | .project' ${OCLOUD_CONFIG});
    PUBIP=$(oci-public-ip -g);

    oci dns zone list \
    --compartment-id $COMPARTMENT \
    --output table \
    --query "data[*].{Name:name, State:\"lifecycle-state\", OCID:id}";

    read -e -p "Please select the zone by OCID: " ZONE;

    FQDN=$(oci dns zone get \
    --zone-name-or-id $ZONE \
    --compartment-id $COMPARTMENT \
    --raw-output \
    --query data.name);

    PUBURL=${PROJECT}.${FQDN};
    
    echo "Creating DNS entry ${PUBURL} for ${PUBIP}";

    oci dns record rrset update \
    --domain "${PUBURL}" \
    --zone-name-or-id $ZONE \
    --rtype 'A' \
    --force \
    --items '[{"domain":"'"${PUBURL}"'","rdata":"'"${PUBIP}"'","rtype":"A","ttl":86400}]'

    oci dns record zone get \
    --zone-name-or-id $ZONE \
    --compartment-id $COMPARTMENT \
    --all \
    --output table \
    --sort-order ASC \
    --query "data.items[*].{Domain:domain, RTYPE:rtype, TTL:ttl}";
}

create_config() {
    read -e -p "Provide a name for your project (only letters and numbers, less than 12 characters): " PROJECT;
    if [ "$PROJECT" != "" ]; then 
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g")
    else
        PROJECT_="";
    fi
    TENANCY=$(oci iam compartment list \
    --raw-output \
    --query 'data[0]."compartment-id"');
    wget -P ${HOME}/ https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/project.json;
    [[ ! -f "${HOME}/project.json" ]] && error "Did not create a project file";
    sed -i 's/VAR_TENANCY/'"${TENANCY}"'/' ${HOME}/project.json; 
    sed -i 's/VAR_PROJECT/'"${PROJECT}"'/' ${HOME}/project.json;
    success "configuration file created";
}

select_admin() {
    oci iam user list --all --output table --query 'data[].{Name: "name", OCID: "id"}';
    read -e -p "Select a user by OCID: " USERID;
    EMAIL=$(oci iam user get --user-id ${USERID} | jq -r .data.email);
    USERSELECTION=$(validate_name ${EMAIL});
    read -e -p "Please a linux user name for this admin (only letters and numbers): " -i $USERSELECTION -n 10 USERNAME;
    [[ -z "${USERID}" ]] && error "Did not create an admin";
    sed -i 's/VAR_VERSIONOS/'"${VERSION_OS}"'/' ${HOME}/project.json;
    sed -i 's/VAR_OS/'"${OS}"'/' ${HOME}/project.json;
    sed -i 's/VAR_SHAPE/'"${SHAPE}"'/' ${HOME}/project.json;
    sed -i 's/VAR_NAME/'"${USERNAME}"'/' ${HOME}/project.json;
    sed -i 's;VAR_EMAIL;'"${EMAIL}"';' ${HOME}/project.json;
    sed -i 's/VAR_USER/'"${USERID}"'/' ${HOME}/project.json;
    success "${EMAIL} is the project admin, ${SSH_KEY} created";
}

select_user() {
    oci iam user list \
    --all \
    --output table \
    --query 'data[].{Name: "name", OCID: "id"}';
    read -e -p "Select an admin user by OCID: " USERID;
    EMAIL=$(oci iam user get --user-id ${USERID} | jq -r .data.email);
    USERSELECTION=$(validate_name ${EMAIL});
    read -e -p "Please a linux user name for this admin (only letters and numbers): " -i $USERSELECTION -n 10 USERNAME;
    success "${EMAIL} is the project admin";
}

select_compartment() {
    if [[ -z "${COMPARTMENT}" ]]; then
        while true; do
            read -p "Do you want to use an existing compartment (y/n)? " yn
            case $yn in
                [Yy]* ) update_compartment; break;;
                [Nn]* ) create_compartment; return;;
                * ) echo "Please answer yes or no.";;
            esac
        done
    else
        echo "${COMPARTMENT} already defined";
        return 1;
    fi
}

create_vcn () {
    # the DNS label for the VCN must not contain non-alphanumeric charactes and only 15 characters are alowed at most
    echo_log "creating a VCN"
    COMPARTMENT=$1;
    PROJECT=$2;
    if [ "$PROJECT" != "" ]; then 
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g");
    else
        PROJECT_="default_";
    fi
    DNSLABEL=$(echo "${PROJECT_}vcn" | tr [A-Z] [a-z] | sed "s/\_//g");
    REGION=$(grep region ${HOME}/.oci/config | cut -f 2 -d '=');

    VCN=$(oci network vcn create \
    --wait-for-state AVAILABLE \
    --cidr-block 10.0.0.0/24 \
    --compartment-id $COMPARTMENT \
    --region $REGION \
    --dns-label $DNSLABEL \
    --display-name "${PROJECT_}vcn" | jq --raw-output .data.id);

    echo_log "VCN created, name: ${PROJECT_}vcn, region: ${REGION}";
}

create_rules() {
    declare -a CIDR_RANGES;
    # WAF Ranges
    # CIDR_RANGES+=(130.35.0.0/20 130.35.112.0/22 130.35.120.0/21 130.35.128.0/20 130.35.144.0/20 130.35.16.0/20 130.35.176.0/20 130.35.192.0/19 130.35.224.0/22 130.35.232.0/21 130.35.240.0/20 130.35.48.0/20 130.35.64.0/19 130.35.96.0/20 138.1.0/20 138.1.104.0/22 138.1.128.0/19 138.1.16.0/20 138.1.160.0/19 138.1.192.0/20 138.1.208.0/20 138.1.224.0/19 138.1.32.0/21 138.1.40.0/21 138.1.48.0/21 138.1.64.0/20 138.1.80.0/20 138.1.96.0/21 147.154.0.0/18 147.154.128.0/18 147.154.192.0/20 147.154.208.0/21 147.154.224.0/19 147.154.64.0/20 147.154.80.0/21 147.154.96.0/19 192.157.18.0/23 192.29.0.0/20 192.29.128.0/21 192.29.144.0/21 192.29.16.0/21 192.29.32.0/21 192.29.48.0/21 192.29.56.0/21 192.29.64.0/20 192.29.96.0/20 192.69.118.0/23 198.181.48.0/21 199.195.6.0/23 205.147.88.0/21);
    CIDR_RANGES+=(0.0.0.0/0);

    # Egress rules
    EGRESS_SECURITY='{"destination": "0.0.0.0/0", "destination-type": "CIDR_BLOCK", "protocol": "all", "isStateless": false}'

    # Ingress rules. The script opens these ports both on network level (security list) as well as on O/S level (firewall-cmd)
    declare -a INGRESS_RULES;

    for CIDR in  "${CIDR_RANGES[@]}"; do
        for PORT in "${INGRESS_PORTS[@]}"; do
            security_rule $CIDR $PORT;
        done;
    done;

    INGRESS_SECURITY=$(echo ${INGRESS_RULES[*]} | jq . -s)
}

security_rule() {
    CIDR=$1
    PORT=$2
    cat <<EOF
    {"source": "${CIDR}", "source-type": "CIDR_BLOCK", "protocol": 6, "isStateless": false, "tcp-options": {"destination-port-range": {"max": $PORT, "min": $PORT}}}
EOF
    };

create_subnet () {
    echo_log "creating subnet in ${VCN}";
    COMPARTMENT=$1;
    PROJECT=$2;
    VCN=$3;
    if [ "$PROJECT" != "" ]; then 
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g");
    else
        PROJECT_="default_";
    fi
    REGION=$(grep region ${HOME}/.oci/config | cut -f 2 -d '=');
    SECURITYLIST="";
    EGRESS_SECURITY="";
    INGRESS_SECURITY="";

    create_rules;

    SECURITYLIST=$(oci network security-list create \
    --region $REGION \
    --display-name "${PROJECT_}sl" \
    --vcn-id $VCN \
    --compartment-id $COMPARTMENT \
    --egress-security-rules "[ ${EGRESS_SECURITY} ]" \
    --ingress-security-rules "${INGRESS_SECURITY}" | jq --compact-output [.data.id]);

    SUBNET=$(oci network subnet create \
    --region $REGION \
    --cidr-block 10.0.0.0/25 \
    --compartment-id $COMPARTMENT \
    --display-name "${PROJECT_}sbnt" \
    --vcn-id $VCN \
    --security-list-ids $SECURITYLIST | jq --raw-output .data.id);

    echo_log "created ${SUBNET} and associated security lists, name: ${PROJECT_}sbnt";
}

create_bucket() {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    COMPARTMENT=$(jq -r '.client.configuration[] | .compartment' ${OCLOUD_CONFIG});
    REGION=$(jq -r '.client.configuration[] | .region' ${OCLOUD_CONFIG});
    HOME_REGION=$(jq -r '.client.configuration[] | .home' ${OCLOUD_CONFIG});
    PROJECT=$(jq -r '.client.configuration[] | .project' ${OCLOUD_CONFIG});
    if [ "$PROJECT" != "" ]; then 
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g");
    else
        PROJECT_="";
    fi

    echo_log "creating a storage bucket for ${PROJECT}";

    NAMESPACE=$(oci iam tag-namespace create \
    --compartment-id $COMPARTMENT \
    --region $HOME_REGION \
    --name "${PROJECT_}admin" \
    --wait-for-state ACTIVE \
    --description "Tag Namespace for Terraform deployment management" | jq --raw-output '.data.id');

    success "namespace ${PROJECT_}admin created";

    sleep 5s;

    TAG=$(oci iam tag create \
    --name "terraform" \
    --description "Terraform ressources" \
    --wait-for-state ACTIVE \
    --region ${HOME_REGION} \
    --tag-namespace-id ${NAMESPACE} | jq --raw-output '.data.id');

    success "${TAG} created"
    
    oci os bucket create \
    --compartment-id $COMPARTMENT \
    --region $REGION \
    --name "${PROJECT_}tfstate";
    
    # the tag name definition is not ready yet, so delay the tag definition update of the bucket as a background process
    echo "waiting for the tag definition to get ready ...";
    sleep 60s;

    oci os bucket update \
    --compartment-id $COMPARTMENT \
    --name "${PROJECT_}tfstate" \
    --defined-tags '{ "'"${PROJECT_}admin"'": {"terraform": "statefile"} }';
    
    touch $HOME/terraform.tfstate

    oci os object put \
    --bucket-name "${PROJECT_}tfstate" \
    --file "${HOME}/terraform.tfstate";

    rm $HOME/terraform.tfstate

    success "Bucket created, name: ${PROJECT_}tfstate"
}

create_secretkey() {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    HOME_REGION=$(jq -r '.client.configuration[] | .home' ${OCLOUD_CONFIG});
    USERID=$(jq -r '.client.admin[] | .user' ${OCLOUD_CONFIG});
    CUSTOMER_SECRET_KEY=$(oci iam customer-secret-key create \
    --display-name  "${PROJECT_}tfstate" \
    --user-id $USERID \
    --region=$HOME_REGION);
    ACCESSKEY=$(echo $CUSTOMER_SECRET_KEY | jq -r ".data.id");
    SECRETKEY=$(echo $CUSTOMER_SECRET_KEY | jq -r ".data.key");
    S3KEY=$(jq -r '.client.admin[] | .s3' ${OCLOUD_CONFIG});

    # store credentials in file
    rm -r ~/.aws;
    mkdir -p ~/.aws;
    cat << EOF > ${S3KEY}
[default]
aws_access_key_id=$ACCESSKEY
aws_secret_access_key=$SECRETKEY
EOF
    success "Customer secret key created and stored in ${S3KEY}";
}

create_compartment () {
    NAME=$1;
    ROOT=$(oci iam compartment list \
    --raw-output \
    --query 'data[0]."compartment-id"');

    oci iam compartment create \
    --compartment-id "${ROOT}" \
    --description "${NAME} compartment" \
    --name "${NAME}"  | jq -r .data.id;
}

create_waf() {
    PUBURL=$1;
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    PUBIP=$(oci-public-ip -g);
    COMPARTMENT=$(jq -r '.client.configuration[] | .compartment' ${OCLOUD_CONFIG});
    PROJECT=$(jq -r '.client.configuration[] | .project' ${OCLOUD_CONFIG});
    if [ "$PROJECT" != "" ]; then 
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g");
    else
        PROJECT_="default_";
    fi

    echo "creating web application firewall";

    WAAS=$(oci waas waas-policy create \
    --compartment-id "${COMPARTMENT}" \
    --domain $PUBURL; \
    --display-name "${PROJECT_}waf" \
    --wait-for-state SUCCEEDED | jq -r .data.id);
    [[ -z "${WAAS}" ]] && error "Did not create a web application firewall";
}

launch_instance() {
    PROJECT=$1;
    CONFIG=$2;
    SHAPE=$3;
    ARTIFACT=$(echo ${CONFIG} | cut -f 1 -d '_');
    VCN=$(select_vcn ${COMPARTMENT} ${ARTIFACT} "DESC");
    LOCATION=$(grep region ${HOME}/.oci/config | cut -f 2 -d '-');
    AD=$(availability_domain "${LOCATION}");
    SUBNET=$(select_subnet ${VCN} ${PROJECT});
    IMAGE=$(jq -r .builds[-1].artifact_id ${HOME}/infracoder/artifacts/packer/${ARTIFACT}_${LOCATION}.json);
    SSH_KEY_PUBLIC="${HOME}/id_rsa.pub";

    oci compute instance launch \
    --compartment-id "${COMPARTMENT}" \
    --display-name "${ARTIFACT}_${LOCATION}" \
    --availability-domain "${AD}" \
    --subnet-id "${SUBNET}" \
    --image-id "${IMAGE}" \
    --shape "${SHAPE}" \
    --ssh-authorized-keys-file "${SSH_KEY_PUBLIC}" \
    --assign-public-ip true \
    --wait-for-state RUNNING \
    --query 'data.id' \
    --raw-output;
}

# --- changes and updates ---
rename_compartment () {
    OLD_COMPARTMENT="...";
    VCN=$(oci iam compartment update \
    --compartment-id $OLD_COMPARTMENT \
    --name "${PROJECT_}COMPARTMENT" | jq --raw-output .data.id);
}

enable_communication () {
    COMPARTMENT=$1;
    PROJECT=$2;
    VCN=$3;
    if [ "$PROJECT" != "" ]; then 
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g");
    else
        PROJECT_="default_";
    fi
    REGION=$(grep region ${HOME}/.oci/config | cut -f 2 -d '=');
    
    echo_log "creating an Internet Gateway and adding default route";
    INTERNET_GATEWAY=$(oci network internet-gateway create \
    --region $REGION \
    --compartment-id $COMPARTMENT \
    --is-enabled true \
    --vcn-id $VCN \
    --display-name "${PROJECT_}IGW" | jq --compact-output '. | [{cidrBlock:"0.0.0.0/0",networkEntityId: .data.id}]');

    ROUTE_TABLE=$(oci network route-table list \
    --region $REGION \
    --compartment-id $COMPARTMENT \
    --vcn-id $VCN | jq --raw-output '.data[].id');

    oci network route-table update \
    --region $REGION \
    --rt-id $ROUTE_TABLE \
    --route-rules $INTERNET_GATEWAY \
    --force;
    
    echo_log "Internet gateway created, name: ${PROJECT_}IGW";
}

_transfer_settings() {
    # --- configure oci cli ---
    echo "sudo cat ~/.oci/config : ";
    OCICONFIG=$(sed '/^$/q');
    echo "$OCICONFIG" > /home/$SUDO_USER/.oci/config;
    su -c 'cat $HOME/.ssh/id_rsa.pub | cat >> $HOME/.ssh/authorized_keys' $SUDO_USER
    chmod 600 /home/$SUDO_USER/.ssh/authorized_keys

    # --- configure oci cli ---
    su -c '/home/${SUDO_USER}/bin/oci setup repair-file-permissions --file /home/${SUDO_USER}/.oci/config' $SUDO_USER
    su -c 'export OCI_CLI_SUPPRESS_FILE_PERMISSIONS_WARNING=True' $SUDO_USER
}

_test_communication () {
    echo_log "launching a test server "
    # first get the name of AD1
    AD=$(oci iam availability-domain list --region $REGION -c $COMPARTMENT | jq --raw-output .data[0].name)
    # get the image OCID an image 
    IMAGE=$(oci compute image list --region $REGION --compartment-id $COMPARTMENT | sed "s/-/_/g" | jq --raw-output ".data[] | select(.display_name | startswith(\"$IMAGE_NAME\")).id" | head -n 1)
    
    # launch an instance
    COMPUTE=$(oci compute instance launch --wait-for-state RUNNING --region $REGION -c $COMPARTMENT --availability-domain $AD --shape $SHAPE --display-name "${PROJECT_}Testserver" --image-id $IMAGE --subnet-id "$SUBNET" --assign-public-ip true --metadata "{\"ssh_authorized_keys\": \"$SSH_KEY_PUBLIC\"}" | jq --raw-output .data.id)
    IP_PUBLIC=$(oci compute instance list-vnics --instance-id $COMPUTE | grep public-ip | awk -F'[\"|\"]' '{print $4}')
    
    ## check whether a connection can be established
    echo_log "checking the status of the instance with the public IP address $IP_PUBLIC at $(date) ..."
    nmap -Pn -p22 $IP_PUBLIC | awk '/Host is up/ { print $0 }' | grep 'is up' &> /dev/null
    
    if [ $? == 0 ]; then
        echo_log "Success!! ${PROJECT_}Testserver is up."
        echo_log "Terminating ${PROJECT_}Testserver ..."
        oci compute instance terminate --instance-id $COMPUTE --force
    else
        echo_log "No Success - check your definition file"
    fi
}

# --- retrieve identifier ---
select_vcn () {
    # select_vcn ocid1.compartment.oc1.xxx myproject ASC (or DESC)
    COMPARTMENT=$1
    PROJEKT=$2
    SORT=$3
    if [ "$PROJECT" != "" ]; then
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g")
    else
        PROJECT_="default_";
    fi
    oci network vcn list \
    --compartment-id $COMPARTMENT \
    --region $REGION \
    --display-name ${PROJECT_}vcn \
    --sort-by TIMECREATED \
    --sort-order $SORT | jq --raw-output '.data[0].id';
}

select_subnet () {
    # select_subnet ocid1.vcn.oc1.xxx myproject
    VCN=$1
    PROJECT=$2
    if [ "$PROJECT" != "" ]; then
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g")
    else
        PROJECT_="default_";
    fi

    oci network subnet list \
    --compartment-id $COMPARTMENT \
    --vcn-id $VCN \
    --region $REGION \
    --raw-output \
    --query "data [?\"display-name\"=='${PROJECT_}sbnt'] | [0].id";
}

image_details () {
    IMAGE_DETAILS=$(oci compute image list \
    --operating-system "Oracle Linux" \
    --operating-system-version "7.7" \
    --shape "VM.Standard2.1" \
    --sort-by TIMECREATED \
    --query '[data[0].id, data[0]."display-name"] | join(`\n`,@)' \
    --raw-output)
    
    { read OCID; read DISPLAYNAME; } <<< "${response}"
    # echo $OCID or echo $DISPLAYNAME
}

availability_domain () {
    AD=$(echo ${*} | tr [a-z] [A-Z])    
    oci iam availability-domain list \
    --all \
    --query 'data[?contains(name, `'"${AD}"'`)] | [0].name' \
    --raw-output
}

ocid_vcn () {
    VCN_OCID=$(oci network vcn list \
    --query "data [?\"display-name\"=='${VCN_NAME}'] | [0].id" \
    --raw-output)
}

public_ip () {
    PUBLIC_IP=$(oci compute instance list-vnics \
    --instance-id "${INSTANCE_OCID}" \
    --query 'data[0]."public-ip"' \
    --raw-output)
}

# --- managing artifacts ---
packer_definition () {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    SERVICE_CONFIG=$1;
    ARTIFACT=${SERVICE_CONFIG%_*};
    COMPARTMENT=$(jq -r '.client.configuration[] | .compartment' ${OCLOUD_CONFIG});
    REGION=$(jq -r '.client.configuration[] | .region' ${OCLOUD_CONFIG});
    LOCATION=$(echo ${REGION} | cut -f 2 -d '-');
    AD=$(jq -r '.client.configuration[] | .availability_domain' ${OCLOUD_CONFIG});
    PROJECT=$(jq -r '.client.configuration[] | .project' ${OCLOUD_CONFIG});
    if [ "$PROJECT" != "" ]; then
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g");
    else
        PROJECT_="default_";
    fi
    SHAPE=$(jq -r '.client.instance[] | .shape' ${OCLOUD_CONFIG});
    VCN=$(select_vcn ${COMPARTMENT} ${PROJECT} "DESC");
    SUBNET=$(select_subnet ${VCN} ${PROJECT});
    OS=$(jq -r '.client.instance[] | .os' ${OCLOUD_CONFIG});
    VERSION_OS=$(jq -r '.client.instance[] | .version' ${OCLOUD_CONFIG});
    GITURL=$(jq -r '.client.configuration[] | .git' ${OCLOUD_CONFIG});
    GITNAME=${GITURL##*:};
    REPO=$(echo ${GITNAME##*/} | cut -d "." -f 1);
    ARTIFACT_DIRECTORY=${HOME}/${REPO}/artifacts/packer/${ARTIFACT};
    
    IMAGE=$(oci compute image list \
    --compartment-id "${COMPARTMENT}" \
    --region "${REGION}" \
    --operating-system "${OS}" \
    --operating-system-version "${VERSION_OS}" \
    --shape "${SHAPE}" \
    --sort-by TIMECREATED \
    --raw-output \
    --query 'data[0].id');
    [[ -z "${IMAGE}" ]] && error "Did not receive an Image OCID";

    echo "creating artifact ${ARTIFACT}";

    if [ -d "$ARTIFACT_DIRECTORY" ]; then
        rm -r $ARTIFACT_DIRECTORY;
    fi
    mkdir $ARTIFACT_DIRECTORY;
    wget -P $ARTIFACT_DIRECTORY/ https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/location.pkr.hcl;
    mv $ARTIFACT_DIRECTORY/location.pkr.hcl $ARTIFACT_DIRECTORY/${LOCATION}.pkr.hcl;
    sed -i 's/VAR_COMPARTMENT/'"${COMPARTMENT}"'/' $ARTIFACT_DIRECTORY/${LOCATION}.pkr.hcl;
    sed -i 's/VAR_SUBNET/'"${SUBNET}"'/' $ARTIFACT_DIRECTORY/${LOCATION}.pkr.hcl;
    sed -i 's/VAR_REGION/'"${REGION}"'/' $ARTIFACT_DIRECTORY/${LOCATION}.pkr.hcl;
    sed -i 's/VAR_AD/'"${AD}"'/' $ARTIFACT_DIRECTORY/${LOCATION}.pkr.hcl;
    sed -i 's/VAR_BASEIMAGE/'"${IMAGE}"'/' $ARTIFACT_DIRECTORY/${LOCATION}.pkr.hcl;
    sed -i 's/VAR_NAME/'"${ARTIFACT}"'/' $ARTIFACT_DIRECTORY/${LOCATION}.pkr.hcl;
    sed -i 's/VAR_SHAPE/'"${SHAPE}"'/' $ARTIFACT_DIRECTORY/${LOCATION}.pkr.hcl;
    success "created file ${LOCATION}.pkr.hcl in ${ARTIFACT_DIRECTORY}";
}

packer_sources () {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    echo "creating file sources.pkr.hcl";
    wget -P $ARTIFACT_DIRECTORY/ https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/sources.pkr.hcl;
    sed -i 's/VAR_LOCATION/'"${LOCATION}"'/' $ARTIFACT_DIRECTORY/sources.pkr.hcl;
    success "sources.pkr.hcl created";
}

packer_build () {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    SERVICE_CONFIG=$1;
    EMAIL=$(jq -r '.client.admin[] | .email' ${OCLOUD_CONFIG});
    NAME=$(jq -r '.client.admin[] | .name' ${OCLOUD_CONFIG});
    ARTIFACT=${SERVICE_CONFIG%_*};
    REGION=$(jq -r '.client.configuration[] | .region' ${OCLOUD_CONFIG});
    LOCATION=$(echo ${REGION} | cut -f 2 -d '-');
    echo "creating file ${ARTIFACT}.pkr.hcl";
    wget -P $ARTIFACT_DIRECTORY/ https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/build.pkr.hcl;
    mv ${ARTIFACT_DIRECTORY}/build.pkr.hcl $ARTIFACT_DIRECTORY/${ARTIFACT}.pkr.hcl;
    sed -i 's/VAR_LOCATION/'"${LOCATION}"'/' $ARTIFACT_DIRECTORY/${ARTIFACT}.pkr.hcl;
    sed -i 's/VAR_ARTIFACT/'"${ARTIFACT}"'/' $ARTIFACT_DIRECTORY/${ARTIFACT}.pkr.hcl;
    sed -i 's/VAR_CONF/'"${CONFIG}"'/' $ARTIFACT_DIRECTORY/${ARTIFACT}.pkr.hcl;
    sed -i 's/VAR_NAME/'"${NAME}"'/' $ARTIFACT_DIRECTORY/${ARTIFACT}.pkr.hcl;
    echo_log "${ARTIFACT}.pkr.hcl created";
}

packer_secrets () {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    SERVICE_CONFIG=$1;
    ARTIFACT=${SERVICE_CONFIG%_*};
    echo "creating file secrets.pkr.hcl";
    SECRETS_PKR=${HOME}/${REPO}/artifacts/packer/${ARTIFACT}/secrets.pkr.hcl;
    
    cat << EOF > $SECRETS_PKR
    # --- tenancy ---
    variable "tenancy_ocid" { default = "$(grep tenancy ${HOME}/.oci/config | cut -f 2 -d '=')" }
    
    # --- credentials ---
    variable "user_ocid" { default = "$(grep user ${HOME}/.oci/config | cut -f 2 -d '=')" }
    variable "fingerprint" { default = "$(grep fingerprint ${HOME}/.oci/config | cut -f 2 -d '=')" }
EOF
    success "secrets.pkr.hcl created";
}

packer_exec() {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    SERVICE_CONFIG=$1;
    ARTIFACT=${SERVICE_CONFIG%_*};
    echo "creating artifact ${ARTIFACT}";
    BUILD_DIRECTORY=${HOME}/${REPO}/artifacts/packer
    cd $BUILD_DIRECTORY && packer build ${ARTIFACT_DIRECTORY};
    success "artifact ${ARTIFACT} created";
}

# --- terraform ---
terraform_setup() {
    OCLOUD_CONFIG="/etc/ocloud/project.json";
    [[ ! -f "${OCLOUD_CONFIG}" ]] && error "project config missing";
    REGION=$(jq -r '.client.configuration[] | .region' ${OCLOUD_CONFIG});
    PROJECT=$(jq -r '.client.configuration[] | .project' ${OCLOUD_CONFIG});
    if [ "$PROJECT" != "" ]; then
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g");
    else
        PROJECT_="default_";
    fi
    GITURL=$(jq -r '.client.configuration[] | .git' ${OCLOUD_CONFIG});
    GITNAME=${GITURL##*:};
    REPO=$(echo ${GITNAME##*/} | cut -d "." -f 1);
    NAMESPACE=$(oci os ns get | jq .data --raw-output);
    AWS_ACCESS_KEY_ID=$(cat ~/.aws/credentials | grep aws_access_key_id | cut -f2 -d '=')
    AWS_SECRET_ACCESS_KEY=$(cat ~/.aws/credentials | grep aws_secret_access_key | sed 's/aws_secret_access_key=//g')
    VERSION_TF="= 0.12.24";
    MAIN_TF=${HOME}/${REPO}/topology/main.tf;
    TERRAFORM_TF=${HOME}/${REPO}/topology/terraform.tf;
    PROVIDER_TF=${HOME}/${REPO}/topology/provider.tf;
    SECRETS_TF=${HOME}/${REPO}/topology/secrets.tf;
    DATASOURCES_TF=${HOME}/${REPO}/topology/datasources.tf;
    VARS_TF=${HOME}/${REPO}/topology/vars.tf;

    echo "creating file main.tf";
    touch $MAIN_TF

    echo "creating file terraform.tf";
    cat << EOF > ${TERRAFORM_TF}
terraform {
  required_version = "${VERSION_TF}"

  backend "s3" {
    endpoint   = "https://${NAMESPACE}.compat.objectstorage.${REGION}.oraclecloud.com"
    region     = "${REGION}"
    bucket     = "${PROJECT_}tfstate"
    key        = "terraform.tfstate"
    shared_credentials_file = "~/.aws/credentials"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    force_path_style            = true
  }
}
EOF
    echo "creating provider.tf";
    cat << EOF > ${PROVIDER_TF}
provider "oci" {
version = "~> 3.58"

tenancy_ocid = var.tenancy_ocid
user_ocid = var.user_ocid
fingerprint = var.fingerprint
private_key_path = var.private_key_path
region = "${REGION}"
disable_auto_retries = false
}
EOF
    echo "creating file secrets.tf";
    cat << EOF > $SECRETS_TF
# --- tenancy ---
variable "tenancy_ocid" { default = "$(grep tenancy ${HOME}/.oci/config | cut -f 2 -d '=')" }
variable "compartment_ocid" { default = "$(jq -r '.client.configuration[] | .compartment' ${OCLOUD_CONFIG})" }
variable "authorized_ips" { default = "0.0.0.0/0" }

# --- credentials ---
variable "user_ocid" { default = "$(grep user ${HOME}/.oci/config | cut -f 2 -d '=')" }
variable "fingerprint" { default = "$(grep fingerprint ${HOME}/.oci/config | cut -f 2 -d '=')" }
variable "private_key_path" { default = "~/.oci/oci_api_key.pem" }
variable "ssh_public_key_path" { default = "~/.ssh/id_rsa.pub" }
variable "ssh_private_key_path" { default = "~/.ssh/id_rsa" }

# --- shared state ---
variable "aws_access_key" { default = "${ACCESSKEY}" }
variable "aws_secret_key" { default = "${SECRETKEY}" }
EOF
    echo "creating file datasources.tf";
    cat << EOF > $DATASOURCES_TF
## --- list of availability domains ---
data "oci_identity_availability_domains" "ads" { compartment_id = var.tenancy_ocid }

## --- remote state ---
data "terraform_remote_state" "module" {
  backend = "s3"
  config = {
    endpoint   = "https://${NAMESPACE}.compat.objectstorage.${REGION}.oraclecloud.com"
    region     = "${REGION}"
    bucket     = "${PROJECT_}tfstate"
    key        = "terraform.tfstate"
    shared_credentials_file = "~/.aws/credentials"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    force_path_style            = true
  }
}
EOF
    echo "creating file vars.tf";
    cat << EOF > $VARS_TF
// See https://docs.cloud.oracle.com/iaas/images/
// Oracle-provided image "Oracle-Autonomous-Linux-7.7-2019.12-0"
variable "image_id" {
  type = map(string)
  default = {
    eu-frankfurt-1 = "ocid1.image.oc1.eu-frankfurt-1.aaaaaaaazhlmuvqljodxke2mr2jwv63qiwbzjrawseiorthib45secauck2a"
  }
}

variable "availability_domain" {
    default = 1
}

variable "instance_shape" {
    default = "VM.Standard2.1"
}

variable "instance_count" {
    default = 1
}

variable "region" {
  default = "${REGION}"
}

variable "state_file" {
  default = "https://${NAMESPACE}.compat.objectstorage.${REGION}.oraclecloud.com"
}

variable "state_bucket" {
  default = "${PROJECT_}tfstate"
}
EOF
    success "terraform setup";
}

topology_build () {
    COMPARTMENT=$1;
    PROJECT=$2;
    REGION=$3;
    if [ "$PROJECT" != "" ]; then
        TOPOLOGY=$(echo $PROJECT | tr [A-Z] [a-z] | sed "s/ /\_/g");
    else
        TOPOLOGY="";
    fi
    BUILD_DIRECTORY=${HOME}/infracoder/topology/${TOPOLOGY};
    
    echo_log "creating directory ${PROJECT}"
    if [ -d "${BUILD_DIRECTORY}" ]; then
        rm -r $BUILD_DIRECTORY;
    fi
    mkdir $BUILD_DIRECTORY;
    
    terraform_provider $REGION $BUILD_DIRECTORY;
    cd $BUILD_DIRECTORY && terraform init;
    OCI_PROVIDER=$(ls ${BUILD_DIRECTORY}/.terraform/plugins/linux_amd64/ | grep "oci");
    ${BUILD_DIRECTORY}/.terraform/plugins/linux_amd64/${OCI_PROVIDER} -command=export -compartment_id $COMPARTMENT -output_path ${BUILD_DIRECTORY};
    echo_log "${PROJECT} created";
}

topology_bootstrap () {
    COMPARTMENT=$1;
    PROJECT=$2;
    if [ "$PROJECT" != "" ]; then
        TOPOLOGY=$(echo $PROJECT | tr [A-Z] [a-z] | sed "s/ /\_/g")
    else
        TOPOLOGY=""
    fi
    BUILD_DIRECTORY=${HOME}/infracoder/topology/${TOPOLOGY}
    declare -a TEMPLATES;
    TEMPLATES+=(core.tf vars.tf availability_domain.tf);

    echo_log "bootstrapping ${PROJECT}";
    for TEMPLATE in ${TEMPLATES[@]}; do
        sed -i 's;variable compartment_ocid {};variable compartment_ocid { default='\"${COMPARTMENT}\"' };' ${BUILD_DIRECTORY}/${TEMPLATE};
    done
    echo_log "${PROJECT} bootstrapped";
}

topology_clean (){
    PROJECT=$1;
    if [ "$PROJECT" != "" ]; then
        TOPOLOGY=$(echo $PROJECT | tr [A-Z] [a-z] | sed "s/ /\_/g")
    else
        TOPOLOGY=""
    fi
    BUILD_DIRECTORY=${HOME}/infracoder/topology/${TOPOLOGY}

    # --- clean topology directory ---
    echo_log "cleaning ${PROJECT}"
    cd ${BUILD_DIRECTORY};
    bash << 'EOF'
    shopt -s extglob
    rm -v !(vars.tf|availability_domain.tf|core.tf)
EOF
    # --- translate terraform files to HCL2 ---
    terraform 0.12upgrade -yes ${BUILD_DIRECTORY};
    echo_log "${PROJECT} cleaned";
}

# --- preparing terraform ---

terraform_instance () {
    echo_log "creating an instance";
    PROJECT=$1;
    SERVICE=$2;
    LOCATION=$3;
    IMAGESOURCE=$4;
    SHAPE=$5;
    USERNAME=$6;
    if [ "$PROJECT" != "" ]; then
        PROJECT_=$(echo ${PROJECT}\_ | tr [A-Z] [a-z] | sed "s/ /\_/g")
        TOPOLOGY=$(echo ${PROJECT} | tr [A-Z] [a-z] | sed "s/ /\_/g")
    else
        PROJECT_="default_";
        TOPOLOGY=""
    fi

    AD=$(availability_domain "${LOCATION}");
    ARTIFACT=$(echo $SERVICE | cut -f 1 -d '_');
    VCN=$(select_vcn ${COMPARTMENT} ${ARTIFACT} "DESC");
    SUBNET=$(select_subnet ${VCN} ${PROJECT});
    SSH_KEY_PUBLIC="${HOME}/id_rsa.pub";
    BUILD_DIRECTORY=${HOME}/infracoder/topology/${PROJECT};

    cat << EOF > ${BUILD_DIRECTORY}/${USER}_${ARTIFACT}.tf

    ## --- admin host ---
    resource "oci_core_instance" "${ARTIFACT}" {
        availability_domain = "${AD}"
        compartment_id      = "${COMPARTMENT}"
        display_name        = "${USERNAME}_${ARTIFACT}"
        shape               = "${SHAPE}"
        
        create_vnic_details {
            subnet_id        = oci_core_subnet.export_${PROJECT_}sbnt.id
            display_name     = "primaryvnic"
            assign_public_ip = "true"
        }
        
        source_details {
            source_type = "image"
            source_id   = "${IMAGESOURCE}"
        }
    }
EOF

cat << EOF > ${BUILD_DIRECTORY}/outputs.tf
    output "InstancePublicIPs" {
        description = "Public IP for instances in the subnet"
        value = oci_core_instance.${ARTIFACT}.*.public_ip
    }
EOF
}

# --- list resources ---

list_images () {
    oci compute image list \
    --region $REGION \
    --compartment-id $COMPARTMENT \
    --output table \
    --all \
    --query "data [*].{Name:\"display-name\", OCID:id}"
}

list_vcn () {
    oci network vcn list \
    --compartment-id $COMPARTMENT \
    --output table \
    --query "data [*].{ID: id, Name: \"display-name\"}"
}

list_platforrm_images () {
    # list platform images, select an image available refer to the following documentation
    # https://blogs.oracle.com/linux/easy-provisioning-of-cloud-instances-on-oracle-cloud-infrastructure-with-the-oci-cli
    oci compute image list \
    --compartment-id $COMPARTMENT \
    --query 'data [*].{OS: "operating-system", Version:"operating-system-version"}' \
    --output table |
    awk '
        BEGIN   { uniq = "sort -u" }
        NR == 1 { hdr = $0 }
        NR < 4  { print; next }
        /^\|/   { print | uniq }
        END     { close(uniq); print hdr}
    '
}

_marketplace_images() {
    # list marketplace images, select an image available refer to the following documentation
    # https://blogs.oracle.com/linux/easy-provisioning-of-cloud-instances-on-oracle-cloud-infrastructure-with-the-oci-cli
    oci compute pic listing list \
    --all \
    --query 'data[?contains("display-name", `'"${IMAGE_NAME}"'`)].join('"'"' '"'"', ["listing-id", "display-name"]) | join(`\n`, @)' \
    --raw-output
}

# --- managing secrets ---
create_vault() {
    echo "creating vault";
    VAULT=$(oci kms management vault create \
    --compartment-id "${COMPARTMENT}" \
    --display-name "${PROJECT_}vault" \
    --wait-for-state ACTIVE \
    --vault-type "DEFAULT" | jq -r .data.id);
    [[ -z "${VAULT}" ]] && error "Did not create a vault";
}

list_vaults() {
    oci kms management vault list \
    --all \
    --compartment-id "${COMPARTMENT}" \
    --output table \
    --query 'data[].{Name: "display-name", OCID: "id", State: "lifecycle-state"}';
}

select_vault() {
    echo "selecting vault";
    VAULT=$(oci kms management vault list \
    --all \
    --compartment-id "${COMPARTMENT}" | jq -r '.data[] | select(."display-name" | startswith('\"${PROJECT_}\"')).id');
    [[ -z "${VAULT}" ]] && error "Did not select a vault";
}

retrieve_endpoint() {
    echo "retrieving endpoint";
    ENDPOINT=$(oci kms management vault get \
    --vault-id "${VAULT}" \
    --raw-output \
    --query 'data."management-endpoint"');
    [[ -z "${ENDPOINT}" ]] && error "Did not retrieve endpoint";
}

create_key() {
    echo "creating key";
    ENCRYPTION_KEY=$(oci kms management key create \
    --compartment-id "${COMPARTMENT}" \
    --display-name "${PROJECT_}default-key" \
    --key-shape '{"algorithm":"AES","length":"16"}' \
    --endpoint "${ENDPOINT}");
    [[ -z "${ENCRYPTION_KEY}" ]] && error "Did not create encryption key";
} 

list_keys() {
    oci kms management key list \
    --all \
    --compartment-id "${COMPARTMENT}" \
    --endpoint "${ENDPOINT}" \
    --output table \
    --query 'data[].{Name: "display-name", OCID: "id"}'; 
}

select_key() {
    echo "selecting key";
    ENCRYPTION_KEY=$(oci kms management key list \
    --all \
    --compartment-id "${COMPARTMENT}" \
    --endpoint "${ENDPOINT}" | jq -r '.data[] | select(."display-name" | startswith('\"${PROJECT_}\"')).id');
    [[ -z "${ENCRYPTION_KEY}" ]] && error "Did not select encryption key";
}

create_secret() {
    # read out with "$SECRET | base64 -d --ignore-garbage"
    read -e -p "Content for the secret: " CONTENT
    SECRET=$(oci vault secret create-base64 \
    --compartment-id "${COMPARTMENT}" \
    --secret-name SSHKEY_PUB \
    --vault-id "${VAULT}" \
    --description "id_rsa.pub" \
    --key-id "${ENCRYPTION_KEY}" \
    --secret-content-content "${CONTENT}");
    [[ -z "${SECRET}" ]] && error "Did not create secret";
}

# --- delete resources ---
delete_route_rules () {
    # delete_route_rules eu-frankfurt-1 ocid1.compartment.oc1.xxx ocid1.vcn.oc1.xxx
    declare -a RTS
    REGION=$1
    COMPARTMENT=$2
    VCN=$3
    array_route_table
    for RT in "${RTS[@]}"; do
        oci network route-table update \
        --rt-id $RT \
        --region $REGION \
        --route-rules '[]' \
        --force;
        sleep 7s;
    done
}

delete_internet_gateways () {
    # delete_internet_gateways eu-frankfurt-1 ocid1.compartment.oc1.xxx ocid1.vcn.oc1.xxx
    declare -a IGS

    REGION=$1
    COMPARTMENT=$2
    VCN=$3

    array_internet_gateway

    for IGW in "${IGWS[@]}"; do
        oci network internet-gateway delete \
        --ig-id $IGW \
        --region $REGION \
        --wait-for-state TERMINATED \
        --force;
        sleep 7s;
    done
}

delete_local-peering-gateway () {
    # delete_local-peering-gateway eu-frankfurt-1 ocid1.localpeeringgateway.oc1.xxx
    REGION=$1 # input the REGION
    ID=$2 # input the ocid

    oci network local-peering-gateway delete \
    --profile DEFAULT \
    --local-peering-gateway-id $ID \
    --region $REGION \
    --wait-for-state TERMINATED \
    --force;
}

delete_drg () {
    # delete_drg eu-frankfurt-1 ocid1.drg.oc1.xxx
    REGION=$1 # input the REGION
    ID=$2 # input the ocid
    
    delete_drg_attachment
    
    oci network drg delete \
    --profile DEFAULT \
    --drg-id $ID \
    --region $REGION \
    --wait-for-state TERMINATED \
    --force;
}

delete_security_lists () {
    # delete_security_lists eu-frankfurt-1 ocid1.compartment.oc1.xxx ocid1.vcn.oc1.xxx
    declare -a SLS

    REGION=$1
    COMPARTMENT=$2
    VCN=$3

    array_security_list
    
    for SL in "${SLS[@]}"; do
        oci network security-list delete \
        --security-list-id $SL \
        --region $REGION \
        --wait-for-state TERMINATED \
        --force;
        sleep 7s;
    done
}

delete_subnets () {
    # delete_subnets eu-frankfurt-1 ocid1.compartment.oc1.xxx ocid1.vcn.oc1.xxx
    declare -a SUBNETS

    REGION=$1
    COMPARTMENT=$2
    VCN=$3

    array_subnet

    for SUBNET in "${SUBNETS[@]}"; do
        oci network subnet delete \
        --subnet-id $SUBNET \
        --region $REGION \
        --wait-for-state TERMINATED \
        --force;
        sleep 7s;
    done
}

delete_dhcp-options () {
    # depends on delete_subnet()
    # delete_dhcp-options eu-frankfurt-1 ocid1.dhcpoptions.oc1.xxx
    REGION=$1 # input the REGION
    ID=$2 # input the ocid

    oci network dhcp-options delete \
    --dhcp-id $ID \
    --region $REGION \
    --wait-for-state TERMINATED \
    --force;
    sleep 7s;
}

delete_vcn () {
    VCN=$1

    oci network vcn delete \
    --vcn-id $VCN \
    --wait-for-state TERMINATED \
    --force;
    sleep 7s;
}

delete_drg_attachment () {
    # delete_subnets eu-frankfurt-1 ocid1.drgattachment.oc1.xxx
    REGION=$1
    DRG=$2

    oci network drg-attachment delete \
    --profile DEFAULT \
    --drg-attachment-id $ID \
    --region $REGION \
    --wait-for-state DETACHED \
    --force;
    sleep 7s;
}

# --- create arrays ---
array_vcn () {
    # select_vcn ocid1.compartment.oc1.xxx myproject
    COMPARTMENT=$1
    PROJEKT=$2

    if [ "$PROJECT" != "" ]; then
        PROJECT_=$(echo $PROJECT\_ | tr [A-Z] [a-z] | sed "s/ /\_/g")
    else
        PROJECT_="default_";
    fi

    VCN_LIST=$(oci network vcn list \
    --compartment-id $COMPARTMENT \
    --display-name ${PROJECT_}vcn \
    --sort-by TIMECREATED \
    --sort-order ASC | jq --raw-output '.data[].id');
    VCNS=($VCN_LIST)
}

array_subnet () {
    SUBNET_LIST=$(oci network subnet list \
    --compartment-id $COMPARTMENT \
    --vcn-id $VCN \
    --all | jq --raw-output '.data[].id')
    SUBNETS=($SUBNET_LIST)
}

array_route_table () {
    RT_LIST=$(oci network route-table list \
    --compartment-id $COMPARTMENT \
    --vcn-id $VCN \
    --all | jq --raw-output '.data[].id')
    RTS=($RT_LIST)
}

array_internet_gateway () {
    IG_LIST=$(oci network internet-gateway list \
    --compartment-id $COMPARTMENT \
    --vcn-id $VCN \
    --all | jq --raw-output '.data[].id')
    IGWS=($IG_LIST)
}

array_security_list () {
    SL_LIST=$(oci network security-list list \
    --compartment-id $COMPARTMENT \
    --vcn-id $VCN \
    --all | jq --raw-output '.data[].id')
    SLS=($SL_LIST)
}