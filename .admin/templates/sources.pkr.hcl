source "oracle-oci" "VAR_LOCATION" {
    tenancy_ocid = var.tenancy_ocid
    region = var.region
    availability_domain = var.availability_domain
    compartment_ocid = var.compartment_ocid
    subnet_ocid = var.subnet_ocid
    user_ocid = var.user_ocid
    fingerprint = var.fingerprint
    base_image_ocid = var.image_ocid
    image_name = var.image_name
    shape = var.shape
    user_data_file = "../../.admin/templates/cloud-init.yml"
    ssh_username = "opc"
}