#!/bin/bash

## Service: Renewal for Let's Encrypt Certificates
## Operator: Oracle
## Location: <URL>
## Software: https://certbot.eff.org/
## Image: Oracle Linux 7.7
## Version: <0.2> 
## Tags: [MGT]

readonly SCRIPTNAME=$(basename $0);

# --- sources ---
source /usr/local/bin/admin.lib.sh
source /usr/local/bin/install.lib.sh
source /usr/local/bin/oci.lib.sh

# --- settings and variables ---
OCLOUD_CONFIG="/etc/ocloud/project.json";
PROJECT=$(jq -r '.client.configuration[] | .project' ${OCLOUD_CONFIG});
FQDN=$(jq -r '.client.configuration[] | .fqdn' ${OCLOUD_CONFIG});
PUBURL="${PROJECT}.${FQDN}";

# --- install modules ---
main () {
    cockpit_cert $PUBURL;
}

# --- execution ---
[[ ! -f "/etc/ocloud/project.json" ]] && error "project file missing";
main "$@"

exit 0