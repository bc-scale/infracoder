#!/bin/bash

PROJECT=$(jq -r '.client.configuration[] | .project' /etc/ocloud/project.json);
COMPARTMENT=$(jq -r '.client.configuration[] | .compartment' /etc/ocloud/project.json);
REGION=$(jq -r '.client.configuration[] | .region' /etc/ocloud/project.json);
IMAGE="VAR_IMAGE";
AD=$(jq -r '.client.configuration[] | .availability_domain' /etc/ocloud/project.json);
SUBNET="VAR_SUBNET";
INITFILE="${HOME}/cloud-init.yml";
SSH_KEY="${HOME}/.ssh/id_rsa";
OS=$(jq -r '.client.instance[] | .os' /etc/ocloud/project.json);
VERSION_OS=$(jq -r '.client.instance[] | .version' /etc/ocloud/project.json);
SHAPE=$(jq -r '.client.instance[] | .shape' /etc/ocloud/project.json);


local SERVERNAME="${PROJECT_}boot-node";
INITFILE="cloud-init.yml";

echo "launching ${SERVERNAME}";
rm -r ${HOME}/*.yml
wget -P ${HOME}/ https://gitlab.com/tboettjer/infracoder/raw/master/.admin/templates/${INITFILE};
[[ ! -f "$HOME/${INITFILE}" ]] && error "Did not create an init file";

IMAGE=$(oci compute image list \
--compartment-id "${COMPARTMENT}" \
--region "${REGION}" \
--operating-system "${OS}" \
--operating-system-version "${VERSION_OS}" \
--shape "${SHAPE}" \
--sort-by TIMECREATED \
--raw-output \
--query 'data[0].id');
[[ -z "${IMAGE}" ]] && error "Did not receive an Image OCID";

SERVER=$(oci compute instance launch \
--compartment-id "${COMPARTMENT}" \
--display-name "${SERVERNAME}" \
--availability-domain "${AD}" \
--region "${REGION}" \
--subnet-id "${SUBNET}" \
--image-id "${IMAGE}" \
--shape "${SHAPE}" \
--ssh-authorized-keys-file ${SSH_KEY}.pub \
--user-data-file "${HOME}/${INITFILE}" \
--assign-public-ip true \
--wait-for-state RUNNING \
--query 'data.id' \
--raw-output);
[[ -z "${SERVER}" ]] && error "Did not launch a Server";

success "launched ${SERVERNAME} in ${COMPARTMENT}\n";

PUBLIC_IP=$(oci compute instance list-vnics \
--instance-id "${SERVER}" \
--region "${REGION}" \
--query 'data[0]."public-ip"' \
--raw-output);
[[ -z "${PUBLIC_IP}" ]] && error "Did not retrieve a public IP address";

echo "Getting public IP ${PUBLIC_IP}, waiting for ssh";
sleep 60;
declare -a TRANSFERS;
TRANSFERS+=("/etc/ocloud/project.json" \
"${HOME}/.ssh/id_rsa" \
"${HOME}/.ssh/id_rsa.pub");
for TRANSFER in "${TRANSFERS[@]}"; do
scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${HOME}/.ssh/id_rsa $TRANSFER opc@${PUBLIC_IP}:/tmp;
while test $? -gt 0
do
        echo "Trying again..."
        sleep 15;
        scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${HOME}/.ssh/id_rsa $TRANSFER opc@${PUBLIC_IP}:/tmp;
done
done

printf "======================================\n";
note "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i ${SSH_KEY} opc@${PUBLIC_IP}"
printf "======================================\n";